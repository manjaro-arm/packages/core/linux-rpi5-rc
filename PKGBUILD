# Contributor: Dave Higham <pepedog@archlinuxarm.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>
# Contributor: Oleg Rakhmanov <oleg@archlinuxarm.org>
# Maintainer: Ray Sherwin <slick517d@gmail.com>

_rcver=6.14
_rcrel=6
pkgrel=1
pkgbase=linux-rpi5-rc
_commit=e6c6ff26a58f9406e040814bdafc267bf7c3d412
_srcname=linux-${_commit}
_kernelname=${pkgbase#linux}
_desc="Raspberry Pi 5 64-bit kernel (release candidate branch)"
pkgver=${_rcver}.rc${_rcrel}
arch=('aarch64')
url="http://www.kernel.org/"
license=('GPL2')
# LTO kernel requires lld depend
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc' 'git' 'pahole' 'zstd')
options=('!strip')
source=("https://github.com/raspberrypi/linux/archive/${_commit}.tar.gz"
#source=($_srcname.zip::"https://codeload.github.com/raspberrypi/linux/zip/refs/heads/rpi-5.19.y"
        'linux-rpi5-rc.preset'
        'logo_linux_clut224.ppm'
        'config-diff-manjaro'
)

md5sums=('00af35de923520f9baf21e36c4c051af'
         '310a462350c87d3a2abed97e63986d34'
         '7f7ddadea6f4a7d3017380cb83b95b5e'
         'bd9efb17aaee2920d0146db78915c738')

prepare() {
  cd "${srcdir}/${_srcname}"
  make bcm2712_defconfig
  cat ../config-diff-manjaro >> .config

  ### Enable SCHED_EXT
        scripts/config -e SCHED_CLASS_EXT

  # add pkgrel to extraversion
  sed -ri "s|^(EXTRAVERSION =)(.*)|\1 \2-${pkgrel}|" Makefile

  # don't run depmod on 'make install'. We'll do this ourselves in packaging
  sed -i '2iexit 0' scripts/depmod.sh

  # Add Manjaro Mascot for cpu core count at boot
  cp ../logo_linux_clut224.ppm drivers/video/logo/
}

build() {
  cd "${srcdir}/${_srcname}"

  # get kernel version
  make prepare

  # load configuration
  # Configure the kernel. Replace the line below with one of your choice.
  #make menuconfig # CLI menu for configuration
  #make nconfig # new CLI menu for configuration
  #make xconfig # X-based configuration
  #make oldconfig # using old config from previous kernel version
  #make bcmrpi_defconfig # using RPi defconfig
  # ... or manually edit .config

  # Copy back our configuration (use with new kernel version)
  #cp ./.config /var/tmp/${pkgbase}.config

  ####################
  # stop here
  # this is useful to configure the kernel
  #msg "Stopping build"
  #return 1
  ####################

  #yes "" | make config

  # LTO kernel compile
  # make LLVM=1 LLVM_IAS=1 ${MAKEFLAGS} Image modules dtbs

  make ${MAKEFLAGS} Image.gz modules dtbs
  make -C tools/bpf/bpftool vmlinux.h feature-clang-bpf-co-re=1  
}

_package() {
  pkgdesc="The Linux Kernel and modules - ${_desc}"
  depends=('coreutils' 'kmod' 'initramfs' 'firmware-raspberrypi' 'raspberrypi-bootloader' 'scx-scheds')
  optdepends=('wireless-regdb: Set the correct wireless channels of your country'
              'linux-firmware: Extra firmware not provided by RPi or kernel')
  provides=("linux=${pkgver}")
  conflicts=('linux-rpi5' 'linux-rpi5-mainline')
  install=${pkgname}.install
  replaces=('linux-raspberrypi-latest')

  cd "${srcdir}/${_srcname}"

  KARCH=arm64

  # get kernel version
  _kernver="$(make kernelrelease)"
  _basekernel=${_kernver%%-*}
  _basekernel=${_basekernel%.*}

  mkdir -p "${pkgdir}"/{boot,usr/lib/modules}
#  make INSTALL_MOD_PATH="${pkgdir}/usr" modules_install
  ZSTD_CLEVEL=19 make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 modules_install

  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-5-b.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-500.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-cm5-cm4io.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-cm5-cm5io.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-cm5l-cm4io.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712-rpi-cm5l-cm5io.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/dts/broadcom/bcm2712d0-rpi-5-b.dtb "${pkgdir}/boot"
  cp arch/$KARCH/boot/Image.gz "${pkgdir}/boot/kernel_2712.img"
  cp .config "${pkgdir}/boot/config-${_kernver}"

  # make room for external modules
  local _extramodules="extramodules-${_basekernel}${_kernelname}"
  ln -s "../${_extramodules}" "${pkgdir}/usr/lib/modules/${_kernver}/extramodules"

  # add real version for building modules and running depmod from hook
  echo "${_kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  # remove build and source links
  rm "${pkgdir}"/usr/lib/modules/${_kernver}/build

  # now we call depmod...
  depmod -b "${pkgdir}/usr" -F System.map "${_kernver}"

  # sed expression for following substitutions
  local _subst="
    s|%PKGBASE%|${pkgbase}|g
    s|%KERNVER%|${_kernver}|g
    s|%EXTRAMODULES%|${_extramodules}|g
  "

  # install mkinitcpio preset file
  sed "${_subst}" ../linux-rpi5-rc.preset |
    install -Dm644 /dev/stdin "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # rather than use another hook (90-linux.hook) rely on mkinitcpio's 90-mkinitcpio-install.hook
  # which avoids a double run of mkinitcpio that can occur
  install -d "${pkgdir}/usr/lib/firmware/"
  echo "dummy file to trigger mkinitcpio to run" > "${pkgdir}/usr/lib/firmware/${_kernver}"
}

_package-headers() {
  pkgdesc="Header files and scripts for building modules for linux kernel - ${_desc}"
  provides=("linux-headers=${pkgver}")
  depends=('linux-rpi5-rc' 'pahole')
  conflicts=('linux-rpi5-headers' 'linux-rpi5-mainline-headers')

  cd ${_srcname}
  local _builddir="${pkgdir}/usr/lib/modules/${_kernver}/build"
  mkdir -p ${_builddir}/tools/bpf/bpftool
  mkdir -p ${_builddir}/tools/bpf/resolve_btfids

  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers
  cp tools/bpf/bpftool/vmlinux.h ${_builddir}/tools/bpf/bpftool/vmlinux.h
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile

  # required when DEBUG_INFO_BTF_MODULES is enabled
  if [ -f tools/bpf/resolve_btfids/resolve_btfids ]; then
      cp tools/bpf/resolve_btfids/resolve_btfids ${_builddir}/tools/bpf/resolve_btfids/resolve_btfids
  fi
  
  mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  install -Dt "${_builddir}/arch/${KARCH}" -m644 arch/${KARCH}/Makefile
  install -Dt "${_builddir}/arch/${KARCH}/kernel" -m644 arch/${KARCH}/kernel/asm-offsets.s

  cp -t "${_builddir}/arch/${KARCH}" -a arch/${KARCH}/include

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */${KARCH}/ ]] && continue
    rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # remove now broken symlinks
  find -L "${_builddir}" -type l -printf 'Removing %P\n' -delete

  # Fix permissions
  #chmod -R u=rwX,go=rX "${_builddir}"

  # strip scripts directory
  local _binary _strip
  while read -rd '' _binary; do
    case "$(file -bi "${_binary}")" in
      *application/x-sharedlib*)  _strip="${STRIP_SHARED}"   ;; # Libraries (.so)
      *application/x-archive*)    _strip="${STRIP_STATIC}"   ;; # Libraries (.a)
      *application/x-executable*) _strip="${STRIP_BINARIES}" ;; # Binaries
      *) continue ;;
    esac
    /usr/bin/strip ${_strip} "${_binary}"
  done < <(find "${_builddir}/scripts" -type f -perm -u+w -print0 2>/dev/null)
}

pkgname=("${pkgbase}" "${pkgbase}-headers")
for _p in ${pkgname[@]}; do
  eval "package_${_p}() {
    _package${_p#${pkgbase}}
  }"
done
